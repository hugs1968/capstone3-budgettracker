//This is a helper function which will randomize the background color for each brand in the chart. It will randomize a hex number.

const colorRandomizer = () => {

	//creates a random number which will be converted into a string.
	//but with the toString method and a number given as an argument to it, it converts this number into a hex decimal.
	//.floor() rounds of the number to the closest integer that is the largest equal or less than the number
	//5.95 = 5
	return Math.floor(Math.random()*16777215).toString(16)


}

export { colorRandomizer }