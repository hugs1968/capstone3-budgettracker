import {useEffect,useState,useContext} from 'react'
import styles from './Register.module.css'
import Image from 'next/image'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Link from 'next/link'
import UserContext from '../../UserContext'
import Navbar from 'react-bootstrap/Navbar';
import Router from 'next/router'
import {Container} from 'react-bootstrap'

export default function Register() {

	const {user} = useContext(UserContext)

	const [firstName, setFirstName] = useState("")
	const [lastName, setLastName] = useState("")
	const [email, setEmail] = useState("")
	const [password1, setPassword1] = useState("")
	const [password2, setPassword2] = useState("")
	const [isActive, setIsActive] = useState(false)



useEffect(()=>{	



	if((firstName !== '' && lastName !== '' &&  email !== '' && password1 !== '' && password2 !=='') && (password2 === password1)){

		setIsActive(true)

	} else {

		setIsActive(false)

	}




},[firstName,lastName,email,password1,password2])




function registerUser(e){

		e.preventDefault()

		console.log('Thank you for registering.')

		setFirstName('')
		setLastName('')
		setEmail('')
		setPassword1('')
		setPassword2('')

		fetch('https://boiling-oasis-07638.herokuapp.com/api/users/email-exists', {

			method: 'POST',
			headers: {								
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email

			})
			
		})
		.then(res => res.json())
		.then(data => {

			if(data === false){

				fetch('https://boiling-oasis-07638.herokuapp.com/api/api/users/', {

					method: "POST",
					headers: {

						"Content-Type": "application/json"

					},
					body: JSON.stringify({

						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1

					})

				})
				.then(res => res.json())
				.then(data => {
					if (data === true){

						alert("Registration Successful")
						Router.push('/login')
						

					} else {

						alert("Registration Failed")

					}

				})

			} else {

				alert("Email Already Exists.")

			}

		})

	}




	return (


			<div className={styles.section}>
				<div className={styles.contentBx}>
					<div className={styles.formBx}>
						<h2>Register</h2>						
						<Form onSubmit={(e)=> registerUser(e)}>							
							<Form.Group className={styles.inputBx} controlId="userFirstName">
								<Form.Label className={styles.span}>First Name</Form.Label>
								<Form.Control className={styles.input} type="text" placeholder="Enter Your First Name" value={firstName} onChange={e => setFirstName(e.target.value)} required />
							</Form.Group>
							<Form.Group className={styles.inputBx}>
								<Form.Label className={styles.span}>Last Name</Form.Label>
								<Form.Control className={styles.input} type="text" placeholder="Enter Your Last Name" value={lastName} onChange={e => setLastName(e.target.value)} required />
							</Form.Group>
							<Form.Group className={styles.inputBx}>
								<Form.Label className={styles.span}>Email Address</Form.Label>
								<Form.Control className={styles.input} type="email" placeholder="Enter Your Email Address" value={email} onChange={e => setEmail(e.target.value)} required />
							</Form.Group>
							<Form.Group controlId="password1" className={styles.inputBx}>
								<Form.Label>Password:</Form.Label>
								<Form.Control type="password" placeholder="Password" onChange={e => setPassword1(e.target.value)} required/>
							</Form.Group>
							<Form.Group controlId="password2" className={styles.inputBx}>
								<Form.Label>Confirm Password:</Form.Label>
								<Form.Control type="password" placeholder="Verify Password" onChange={e => setPassword2(e.target.value)} required/>
							</Form.Group>
							{

								isActive 
								? <div className={styles.inputBx}>
								<input type="submit" />
									</div>
								: <div className={styles.inputBx}>
								<input type="submit" disabled />
									</div>

							}							
							<div className={styles.inputBx}>
								<p>Already have an account? <a href="/login">Log in</a></p>
							</div>							
						</Form>
					</div>
				</div>
				<div className={styles.imgBx}>
					<img className={styles.imageBg} src="/register-bg.jpg" alt="login background" />
				</div>
			</div>	


		)


}