import {Fragment,useState,useEffect} from 'react'
import AppHelper from '../../app_helper.js'
//import PieBreakdown from './../../components/PieChart'
import {Row,Col} from 'react-bootstrap'
import {Container} from 'react-bootstrap'
import moment from 'moment'
import Form from 'react-bootstrap/Form'
import {Pie} from 'react-chartjs-2'
import {colorRandomizer} from '../../helpers/colorRandomizer'

import styles from './CategoryBreakdown.module.css'
import UserContext from '../../UserContext'

import { FaUnlink } from 'react-icons/fa'
import Image from 'next/image'
import { MdDashboard } from 'react-icons/md'
import { IoHome } from "react-icons/io5";
import { IoListCircle } from "react-icons/io5";
import { IoBarChartSharp } from "react-icons/io5";
import { ImList2 } from "react-icons/im";
import { ImIndentDecrease } from "react-icons/im";
import { MdLibraryBooks } from 'react-icons/md'
import { FaCalendarMinus } from "react-icons/fa";
import { GiChart } from "react-icons/gi";
import { GiHamburgerMenu } from "react-icons/gi";
import { BsFillPieChartFill } from "react-icons/bs";
import { BsArrowRightShort } from "react-icons/bs";
import { BsFillCaretDownFill } from "react-icons/bs";
import { FiMenu } from "react-icons/fi";
import { VscSearch } from "react-icons/vsc";


export default function CategoryBreakdown(){

	const [dateFrom,setDateFrom] = useState('')
	const [dateTo,setDateTo] = useState('')
	const [bgColors,setBgColors] = useState([])

	//categoryData is an array state that we pass as props to piechart
	const [categoryData, setCategoryData] = useState([])

	//filteredData is an array state filtered according to the dates set 
	const [filteredData, setFilteredData] = useState([])
	const [names, setNames] = useState([])
	const [dataPerCategories, setDataPerCategories] = useState([])

	//const [breakdown, setBreakdown] = useState([])


	//this will set the dateFrom and dateTo 
	useEffect(()=> {
		let today = new Date();
		setDateTo(moment(today).format('YYYY-MM-DD'))
		setDateFrom(moment(today).subtract(31, 'days').format('YYYY-MM-DD'))
	},[])

	//this will fetch all the data at initial render. and store the data in a state
	useEffect(()=>{
		
		const datum = {
			headers: {
				Authorization: `Bearer ${AppHelper.getAccessToken()}`
			}
		}

		fetch(`${AppHelper.API_URL}/users/records`, datum)
		.then(res => res.json())
		.then(data => {
			//console.log(data)			
			setCategoryData(data)
		})

		
		
	},[])

	//this will filter the data once the all the records are retrieved, and every time the dates are changed and store them in an array state
	useEffect(()=>{
		let tempArr = []

		categoryData.forEach(record => {
			//console.log(record)
			let recordDate = moment(record.dateMade).format('YYYY-MM-DD')
			if(recordDate >= dateFrom && recordDate <= dateTo){
				//console.log(record)
				tempArr.push(record)
			}				
		})
		//console.log(tempArr)
		setFilteredData(tempArr)

	},[categoryData, dateFrom, dateTo])

	//console.log(categoryData)
	//console.log(filteredData)

	//this will get all the names of categories made by user everytime filteredData is changed
	useEffect(()=>{
		//console.log(data)

		if(filteredData.length > 0){
			//console.log(data)
			let tempCategories = [] 
			filteredData.forEach(element => {
				//console.log(element)
				if(!tempCategories.find(category => category === element.categoryName)){
					tempCategories.push(element.categoryName)
				}
			})
			setNames(tempCategories)
		}

	},[filteredData])

	//console.log(names)
	
	//this will add all the records that fall within the same categories.
	useEffect(()=>{

		setDataPerCategories(names.map(name => {

			let total = 0

			filteredData.forEach(element => {
				if(element.categoryName === name){
					total = total + element.amount
				}
			})
			//console.log(total)
			return total
		}))

		setBgColors(names.map(() => `#${colorRandomizer()}`))

	},[names])

	
	return(

			<>
			<input type="checkbox" id="navToggle" className={styles.navToggle} />
			<div className={styles.sidebar}>
				<div className={styles.sidebarBrand}>
					<h6><span className={styles.accusoftLogo}><BsFillPieChartFill /></span> <span>Budget Buddy</span></h6>
				</div>

				<div className={styles.sidebarMenu}>
					<ul>
						<li>
							<a href="/dashboard" className={styles.activeDashboard}><span className={styles.menu}><IoHome /></span><span>Dashboard</span></a>
						</li>						
						<li>
							<a href="/categories" className={styles.activeCategories}><span className={styles.menu}><ImList2 /></span><span>Categories</span></a>
						</li>
						<li>
							<a href="/records" className={styles.activeRecords}><span className={styles.menu}><MdLibraryBooks /></span><span>Records</span></a>
						</li>
						<li>
							<a href="/charts/monthly-income" className={styles.activeIncome}><span className={styles.menu}><IoBarChartSharp /></span><span>Monthly Income</span></a>
						</li>
						<li>
							<a href="/charts/monthly-expense" className={styles.activeExpense}><span className={styles.menu}><FaCalendarMinus />	</span><span>Monthly Expense</span></a>
						</li>
						<li>
							<a href="/charts/balance-trend" className={styles.activeTrend}><span className={styles.menu}><GiChart /></span><span>Trend</span></a>
						</li>
						<li>
							<a href="/charts/category-breakdown" className={styles.activeBreak}><span className={styles.menu}><BsFillPieChartFill /></span><span>Budget Breakdown</span></a>
						</li>
					</ul>
				</div>
			</div>


			<div className={styles.mainContent}>
				<div className={styles.header}>

						<h2>
							<label htmlFor="navToggle">
								<span className={styles.laBars}><FiMenu /></span>
							</label>

							Category Breakdown
						</h2>

						<div className={styles.userWrapper}>
							<img src="/avatar.png" alt="" className="" width="40px" height="40px" />
							<div>
								<h4>John Doe</h4>
								<small>Super admin</small>															
							</div>
							<div className={styles.userDropdown}>
				          		<button><BsFillCaretDownFill className={styles.dropdownButton} /></button>
					          <ul>					            
						        <li><a href="/logout">Logout</a></li>	
					          </ul>
					        </div>						
						</div>						
							
				</div>

				<div className={styles.main}>

					<Container className={styles.chartContainer}>
					
						<div className="mt-5 pt-4 mb-5">
						<Container>
						<Fragment>
						<h2 className="text-center">Category Breakdown</h2>
					
						<Form>
							<Row>
								<Col>				
									<Form.Group>
										<Form.Label>From</Form.Label>
										<Form.Control type="date" 
										className="form-control"
										value={dateFrom}
										onChange={e => setDateFrom(e.target.value)}
										placeholder="Start Date" />
									</Form.Group>
								</Col>
								<Col>
									<Form.Group>
										<Form.Label>To</Form.Label>
										<Form.Control type="date" 
										className="form-control" 
										value={dateTo} 
										onChange={e => setDateTo(e.target.value)}
										placeholder="End Date" />
									</Form.Group>
								</Col>				
							</Row>
						</Form>			
						<hr />
						<Pie data = {{

							labels: names,
							datasets:[{

								data: dataPerCategories,//array of numbers
								backgroundColor: bgColors
							
							}]

						}}/>

						</Fragment>
						</Container>
						</div>
					</Container>

					   

				</div>



			</div>

			</>
		)
}