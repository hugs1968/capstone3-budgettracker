import {Fragment,useState,useEffect} from 'react'
import {Row,Col} from 'react-bootstrap'
import Form from 'react-bootstrap/Form'
import LineChartTrend from './../../components/charts/LineChart'
import {Container} from 'react-bootstrap'
import AppHelper from '../../app_helper.js'
import moment from 'moment'


export default function BalanceTrend(){

	const [dateFrom, setDateFrom] = useState('')
	const [dateTo, setDateTo] = useState('')
	const [allRecords, setAllRecords] = useState([])
	const [filteredBalance, setFilteredBalance] = useState([])
	const [dates, setDates] = useState([])

	useEffect(()=> {
		let today = new Date();
		setDateTo(moment(today).format('YYYY-MM-DD'))
		setDateFrom(moment(today).subtract(31, 'days').format('YYYY-MM-DD'))
		//console.log(dateFrom)
		//console.log(dateTo)
	},[])

	useEffect(() => {

		const datum = {
			headers: {
				Authorization: `Bearer ${AppHelper.getAccessToken()}`
			}
		}

		fetch(`${AppHelper.API_URL}/users/records`, datum)
		.then(res => res.json())
		.then(data => {
			console.log(data.balance)
			setAllRecords(data.balance)
		})
	},[])

	useEffect(()=>{
		let tempArr = []

		allRecords.forEach(record => {
			let recordDate = moment(record.dateMade).format('YYYY-MM-DD')
			if(recordDate >= dateFrom && recordDate <= dateTo){
				//console.log(record)
				tempArr.push(record.balance)
			}	
		})
		//console.log(tempArr)
		setFilteredBalance(tempArr)

		tempArr = []

		allRecords.forEach(record => {
			let recordDate = moment(record.dateMade).format('YYYY-MM-DD')
			if(recordDate >= dateFrom && recordDate <= dateTo){
				//console.log(record)
				tempArr.push(recordDate)
			}	
		})
		//console.log(tempArr)
		setDates(tempArr)
	},[allRecords, dateFrom, dateTo])


	return(
 
			<Container className="mt-5">
			<Fragment>
			<h2>Balance Trend</h2>

		
			<Form>
				<Row>
					<Col>				
						<Form.Group>
							<Form.Label>From</Form.Label>
							<Form.Control 
							type="date" 
							className="form-control" 
							value={dateFrom} 
							onChange={e => setDateFrom(e.target.value)}/>
						</Form.Group>
					</Col>
					<Col>
						<Form.Group>
							<Form.Label>To</Form.Label>
							<Form.Control 
							type="date" 
							className="form-control" 
							value={dateTo} 
							onChange={e => setDateTo(e.target.value)}/>
						</Form.Group>
					</Col>				
				</Row>
			</Form>			
			<hr />
			<LineChartTrend data={filteredBalance} dates={dates}/>

			</Fragment>
			</Container>

		)
}
