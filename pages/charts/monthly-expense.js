import {useState,useEffect,useContext} from 'react'
import AppHelper from '../../app_helper.js'
import BarChart from './../../components/charts/BarChart'
import Sidenav from '../../components/sidenav/Sidenav'
import styles from './MonthlyExpense.module.css'
import Container from 'react-bootstrap/Container'
import UserContext from '../../UserContext'

import { FaUnlink } from 'react-icons/fa'
import Image from 'next/image'
import { MdDashboard } from 'react-icons/md'
import { IoHome } from "react-icons/io5";
import { IoListCircle } from "react-icons/io5";
import { IoBarChartSharp } from "react-icons/io5";
import { ImList2 } from "react-icons/im";
import { ImIndentDecrease } from "react-icons/im";
import { MdLibraryBooks } from 'react-icons/md'
import { FaCalendarMinus } from "react-icons/fa";
import { GiChart } from "react-icons/gi";
import { GiHamburgerMenu } from "react-icons/gi";
import { BsFillPieChartFill } from "react-icons/bs";
import { BsArrowRightShort } from "react-icons/bs";
import { BsFillCaretDownFill } from "react-icons/bs";
import { FiMenu } from "react-icons/fi";
import { VscSearch } from "react-icons/vsc";




export default function MonthlyExpense(){

	const {user} = useContext(UserContext)

	console.log(user)

	const [monthlyExpense, setMonthlyExpense] = useState([])
	const [userInfo, setUserInfo] = useState([])

	//this will fetch all the data at initial render. and store the data in a state
	useEffect(()=>{
		let tempArr = []
		const datum = {
			headers: {
				Authorization: `Bearer ${AppHelper.getAccessToken()}`
			}
		}

		fetch(`${AppHelper.API_URL}/users/records`, datum)
		.then(res => res.json())
		.then(data => {
			console.log(data)			
			data.map(record => {
				if(record.categoryType === "Expense"){
					//const date = moment(record.dateMade).format('MMMM')
					tempArr.push(record)
				}
			})
			//console.log(tempArr)
			setMonthlyExpense(tempArr)
		})
	},[])



	//console.log(monthlyExpense)

	return(
		<>

			<input type="checkbox" id="navToggle" className={styles.navToggle} />
			<div className={styles.sidebar}>
				<div className={styles.sidebarBrand}>
					<h6><span className={styles.accusoftLogo}><BsFillPieChartFill /></span> <span>Budget Buddy</span></h6>
				</div>

				<div className={styles.sidebarMenu}>
					<ul>
						<li>
							<a href="/dashboard" className={styles.activeDashboard}><span className={styles.menu}><IoHome /></span><span>Dashboard</span></a>
						</li>						
						<li>
							<a href="/categories" className={styles.activeCategories}><span className={styles.menu}><ImList2 /></span><span>Categories</span></a>
						</li>
						<li>
							<a href="/records" className={styles.activeRecords}><span className={styles.menu}><MdLibraryBooks /></span><span>Records</span></a>
						</li>
						<li>
							<a href="/charts/monthly-income" className={styles.activeIncome}><span className={styles.menu}><IoBarChartSharp /></span><span>Monthly Income</span></a>
						</li>
						<li>
							<a href="/charts/monthly-expense" className={styles.activeExpense}><span className={styles.menu}><FaCalendarMinus />	</span><span>Monthly Expense</span></a>
						</li>
						<li>
							<a href="/charts/balance-trend" className={styles.activeTrend}><span className={styles.menu}><GiChart /></span><span>Trend</span></a>
						</li>
						<li>
							<a href="/charts/category-breakdown" className={styles.activeBreak}><span className={styles.menu}><BsFillPieChartFill /></span><span>Budget Breakdown</span></a>
						</li>
					</ul>
				</div>
			</div>


			<div className={styles.mainContent}>
				<div className={styles.header}>

						<h2>
							<label htmlFor="navToggle">
								<span className={styles.laBars}><FiMenu /></span>
							</label>

							Monthly Expense
						</h2>

						<div className={styles.userWrapper}>
							<img src="/avatar.png" alt="" className="" width="40px" height="40px" />
							<div>
								<h4>John Doe</h4>
								<small>Super admin</small>															
							</div>
							<div className={styles.userDropdown}>
				          		<button><BsFillCaretDownFill className={styles.dropdownButton} /></button>
					          <ul>					            
						        <li><a href="/logout">Logout</a></li>	
					          </ul>
					        </div>						
						</div>						
							
				</div>

				<div className={styles.main}>

					<Container className={styles.chartContainer}>
					<h6 className="text-center">This chart shows your monthly expenses.</h6>
					<BarChart data={monthlyExpense}/>
					</Container>

					   

				</div>



			</div>

			
		</>
		)
}