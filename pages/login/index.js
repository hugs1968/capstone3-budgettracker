import styles from './Login.module.css'
import {useEffect,useState,useContext} from 'react'
import Image from 'next/image'
import Form from 'react-bootstrap/Form'
import UserContext from '../../UserContext'
import Router from 'next/router'
import AppHelper from '../../app_helper'

export default function Login() {


const {user,setUser} = useContext(UserContext)

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [isActive, setIsActive] = useState(false)
	const [willRedirect,setWillRedirect] = useState(false)



useEffect(()=>{		

		if((email !== '' && password !== '')){

			setIsActive(true)

		} else {

			setIsActive(false)

		}

	},[email,password])

function authenticate(e){

	e.preventDefault()

	fetch(`${AppHelper.API_URL}/users/login`,{

		method: "POST",
		headers: {

			'Content-Type': 'application/json'

		},
		body: JSON.stringify({

			email: email,
			password: password

		})

	})
	.then(res => res.json())
	.then(data => {

		if(data.accessToken){
			
		localStorage.setItem('token', data.accessToken)

		fetch(`${AppHelper.API_URL}/users/details`,{

			headers: {

				Authorization: `Bearer ${data.accessToken}`

			}

		})
		.then(res => res.json())
		.then(data => {

			setUser({

				id: data._id,
				email: data.email				

			})

			Router.push('/dashboard')

		})

		} else {

			alert("Authentication Failed")

		}

		

	})


	setEmail('')
	setPassword('')

}



	return (


			<div className={styles.section}>
				<div className={styles.imgBx}>
					<img className={styles.imageBg} src="/login-bg.jpg" alt="login background" />
				</div>
				<div className={styles.contentBx}>
					<div className={styles.formBx}>
						<h2>Login</h2>
						<Form onSubmit={(e) => authenticate(e)}>							
							<Form.Group className={styles.inputBx} controlId="userEmail">
								<Form.Label className={styles.span}>Email Address</Form.Label>
								<Form.Control className={styles.input} type="email" value={email} onChange={(e) => setEmail(e.target.value)} required />
							</Form.Group>
							<Form.Group className={styles.inputBx} controlId="password1">
								<Form.Label className={styles.span}>Password</Form.Label>
								<Form.Control className={styles.input} type="password" value={password} onChange={(e) => setPassword(e.target.value)} required />
							</Form.Group>
							<div className={styles.remember}>
								<label><input type="checkbox" />Remember me</label>
							</div>
							{

								isActive 
								? <div className={styles.inputBx}>
								<input type="submit" />
							</div>
								: <div className={styles.inputBx}>
									<input type="submit" disabled />
								</div>

							}	


							
							<div className={styles.inputBx}>
								<p>Don't have an account? <a href="/register">Sign up</a></p>
							</div>
						</Form>
					</div>
				</div>
			</div>	


		)


}