import {useState,useEffect,useContext} from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Card from 'react-bootstrap/Card'
import {Row,Col} from 'react-bootstrap'
import UserContext from '../../UserContext'
import AppHelper from '../../app_helper.js'
import Swal from 'sweetalert2'
import Modal from 'react-bootstrap/Modal'
import ModalDialog from 'react-bootstrap/ModalDialog'
import ModalHeader from 'react-bootstrap/ModalHeader'
import ModalTitle from 'react-bootstrap/ModalTitle'
import ModalBody from 'react-bootstrap/ModalBody'
import ModalFooter from 'react-bootstrap/ModalFooter'

import Router from 'next/router'

export default function newCategory(props) {

  const {title, children, openPopup, setOpenPopup} = props;

  const {user} = useContext(UserContext)

  const [categoryName,setCategoryName] = useState('')
  const [categoryType,setCategoryType] = useState('')
  const [isActive,setIsActive] = useState(false)

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);


  useEffect(()=>{

    if(categoryName !== '' && categoryType !== ''){

      setIsActive(true)

    } else {

      setIsActive(false)

    }

  })

  function createCategory(e) {

    e.preventDefault()

    const payload = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${AppHelper.getAccessToken()}`
      },
      body: JSON.stringify({
        categoryName: categoryName,
        categoryType: categoryType
      })
    }

    fetch(`${AppHelper.API_URL}/users/add-category/`, payload)
    .then(res => res.json())
    .then(data => {
      console.log(data)
        if(data){ 
          Swal.fire({
          icon: 'success',
          title: "Category added"
          })
          Router.push('/categories')
        } else {
          Swal.fire({
          icon: 'error',
          title: "There was an error"
          })        

        }  
    })

    //console.log(`The New Category has been added`)

    setCategoryName('')
    setCategoryType('')

  }


  return (


      <div className="mt-5 pt-4 mb-5 container">
      
      <Modal show = {openPopup}>        
          <Modal.Title>
              <h3>New Category</h3>
          </Modal.Title>        

        <Modal.Body>
              <Row className="justify-content-center">
        <Col md={6}>





          
          <Card>
            <Card.Header>Category Information</Card.Header>
            <Card.Body>
              <Form onSubmit={(e)=> createCategory(e)}>
                <Form.Group>
                  <Form.Label>
                    Category Name:
                  </Form.Label>
                  <Form.Control type="text" placeholder="Enter Category Name" value={categoryName} onChange={e => setCategoryName(e.target.value)} required className="form-control" />
                </Form.Group>
                <Form.Group>
                  <Form.Label>
                    Select Category:
                  </Form.Label>
                  <select value={categoryType} onChange={e => setCategoryType(e.target.value)} required className="form-control">
                    <option value="true" disabled="">Select Category</option>                    
                    <option value="Income">Income</option>
                    <option value="Expense">Expense</option>
                  </select>
                </Form.Group>
                <Button type="submit" className="btn btn-primary">Submit</Button>
              </Form>              
            </Card.Body>
          </Card>


        </Col>
        
      </Row>
        </Modal.Body>        
      </Modal>

      




      </div>







    )
}