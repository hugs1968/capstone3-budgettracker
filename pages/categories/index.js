import {useContext,useEffect,useState,Fragment} from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Table from 'react-bootstrap/Table'
import AppHelper from '../../app_helper.js'
import Sidebar from '../../components/sidebar/Sidebar'
import styles from './Categories.module.css'
import newCategory from './new'
import {Col,Row} from 'react-bootstrap'
import Popup from '../../components/modals/Popup'

import { IoHome } from "react-icons/io5";
import { IoListCircle } from "react-icons/io5";
import { IoBarChartSharp } from "react-icons/io5";
import { ImList2 } from "react-icons/im";
import { ImIndentDecrease } from "react-icons/im";
import { MdLibraryBooks } from 'react-icons/md'
import { FaCalendarMinus } from "react-icons/fa";
import { GiChart } from "react-icons/gi";
import { GiHamburgerMenu } from "react-icons/gi";
import { BsFillPieChartFill } from "react-icons/bs";
import { BsArrowRightShort } from "react-icons/bs";
import { BsFillCaretDownFill } from "react-icons/bs";
import { BiLogOut } from "react-icons/bi";
import { FiMenu } from "react-icons/fi";
import { VscSearch } from "react-icons/vsc";


export default function Categories() {

  const [firstName,setFirstName] = useState('')
  const [lastName,setLastName] = useState('')
  const [categoriesRows, setCategoriesRows] = useState([])
  const [openPopup, setOpenPopup] = useState(false)

  useEffect(() => {

    const options = {
      headers: {
        Authorization: `Bearer ${AppHelper.getAccessToken()}`
      }
    }

    fetch(`https://boiling-oasis-07638.herokuapp.com/api/users/categories`, options)
    .then(AppHelper.toJSON)
    .then(data => {
      console.log(data)
      const categoriesAll = data.map(category => {

        return (
          <tr key ={category._id}>
            <td>{category.categoryName}</td>         
            {category.categoryType === "Income"
              ? 
              <td><span className={styles.categoryIncome}>{category.categoryType}</span></td>
              :
              <td><span className={styles.categoryExpense}>{category.categoryType}</span></td>
            }
            
          </tr> 

          )
      })
      setCategoriesRows(categoriesAll)
    })
  },[])



  useEffect(() => {
    const option = {
      headers: {
        Authorization: `Bearer ${AppHelper.getAccessToken()}`
      }
    }

    fetch(`https://boiling-oasis-07638.herokuapp.com/api/users/details`, option)
    .then(AppHelper.toJSON)
    .then(data => {
      //console.log(data)

      setFirstName(data.firstName)
      setLastName(data.lastName)
         
      })
      
  },[])

  return (

    <Fragment>
                <input type="checkbox" id="navToggle" className={styles.navToggle} />
      <div className={styles.sidebar}>
        <div className={styles.sidebarBrand}>
          <h6><span><BsFillPieChartFill /></span><span>Budget Buddy</span></h6>
        </div>

        <div className={styles.sidebarMenu}>
          <ul>
            <li>
              <a href="/dashboard" className={styles.activeDashboard}><span className={styles.menu}><IoHome /></span><span>Dashboard</span></a>
            </li>           
            <li>
              <a href="/categories" className={styles.activeCategories}><span className={styles.menu}><ImList2 /></span><span>Categories</span></a>
            </li>
            <li>
              <a href="/records" className={styles.activeRecords}><span className={styles.menu}><MdLibraryBooks /></span><span>Records</span></a>
            </li>
            <li>
              <a href="/charts/monthly-income" className={styles.activeIncome}><span className={styles.menu}><IoBarChartSharp /></span><span>Monthly Income</span></a>
            </li>
            <li>
              <a href="/charts/monthly-expense" className={styles.activeExpense}><span className={styles.menu}><FaCalendarMinus /></span><span>Monthly Expense</span></a>
            </li>
            <li>
              <a href="/charts/balance-trend" className={styles.activeTrend}><span className={styles.menu}><GiChart /></span><span>Trend</span></a>
            </li>
            <li>
              <a href="/charts/category-breakdown" className={styles.activeBreak}><span className={styles.menu}><BsFillPieChartFill /></span><span>Budget Breakdown</span></a>
            </li>
            <li className="mt-5 pt-5">
              <a href="/charts/category-breakdown" className={styles.activeBreak}><span className={styles.menu}><BiLogOut /></span><span>Logout</span></a>
            </li>
          </ul>
        </div>
      </div>

      <div className={styles.mainContent}>
        <div className={styles.header}>

            <h2>
              <label htmlFor="navToggle">
                <span className={styles.laBars}><FiMenu /></span>
              </label>

              Dashboard
            </h2>

            <div className={styles.userWrapper}>
              <img src="/avatar.png" alt="" className="" width="40px" height="40px" />
              <div>
                <h4>{firstName} {lastName}</h4>
                <small>Super admin</small>                              
              </div>
              <div className={styles.userDropdown}>
                      <button><BsFillCaretDownFill className={styles.dropdownButton} /></button>
                    <ul>                      
                    <li><a href="/logout">Logout</a></li> 
                    </ul>
                  </div>            
            </div>  
        </div>

        <div className={styles.main}>
          

                <div className="pt-3 mb-5 container">
          
                            <div className={styles.categoriesTable}>
                            
                                        <div className={styles.categoryGrid}>
                                          <div className={styles.categoryCard}>
                                            <div>
                                            <Row>
                                              <Col md={6} className={styles.categoryHeader}>
                                              <p>List of Your Budget Description.</p>
                                              </Col>
                                              <Col className={styles.addBtn} md={6}>
                                                <button onClick={() => setOpenPopup(true)} className={styles.raise}>+ Add New</button>
                                              </Col>
                                            </Row>
                                            </div>
                                            <div className={styles.tableResponsive}>


                                              <table>
                                                <thead>
                                                  <tr>
                                                    <th>Description</th>
                                                    <th>Budget Type</th>                      
                                                  </tr>
                                                </thead>

                                                <tbody>
                                                  {categoriesRows}        
                                                </tbody>
                                            </table>
                                            </div>
                                            </div>

                          </div>
                        </div>          
                </div>



        </div>



      </div>

      <Popup
      openPopup = {openPopup}
      setOpenPopup = {setOpenPopup}
      >
      </Popup>

      

      </Fragment>
    )
}



