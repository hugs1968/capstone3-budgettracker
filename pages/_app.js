import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import AppHelper from '../app_helper.js'
import { library } from '@fortawesome/fontawesome-svg-core';
import {useState,useEffect} from 'react'
import {UserProvider} from '../UserContext'

function MyApp({ Component, pageProps }) {

const [user,setUser] = useState({

    id: null,
    email: null   

  })

useEffect(()=>{

      fetch(`${AppHelper.API_URL}/users/details`,{

        headers: {

          Authorization: `Bearer ${localStorage.getItem('token')}`

        }

      })
      .then(res => res.json())
      .then(data => {
          //console.log(data)
          if(data._id){

              setUser({

              id: data._id,
              email: data.email,          

          })

          } else {

              setUser({

              id: null,
              email: null
          })

        }

      })

  }, []) 

  const unsetUser = () => {
    localStorage.clear()
    setUser({
      id: null,
      email: null,
    })
  }

  return (

    <UserProvider value={{user,setUser,unsetUser}}>
      <Component {...pageProps} />
    </UserProvider>

    )
}

export default MyApp
