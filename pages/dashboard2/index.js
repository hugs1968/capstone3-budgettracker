import Sidebar from '../../components/sidebar/Sidebar'
import styles from './Dashboard.module.css'
import DropdownButton from 'react-bootstrap/DropdownButton'
import Dropdown from 'react-bootstrap/Dropdown'

export default function Dashboard() {

	return (

		<div className={styles.dashboard}>

		<Sidebar />


		<div className={styles.mainContent}>


			<div className={styles.header}>
				
				<div className={styles.searchWrapper}>
					<h5>Home</h5>
				</div>

				<div className={styles.socialIcons}>
					
					<div></div>
				</div>

			</div>
		

			<div className={styles.main}>
				
				<h2 className={styles.dashTitle}>Overview</h2>

				<div className={styles.dashCards}>

					<div className={styles.cardSingle}>
						<div className={styles.cardBody}>
							<span className={styles.tiBriefcase}><img src="/briefcase.svg" alt="briefcase icon" /></span>
							<div>
								<h5>Account Balance</h5>
								<h4>$30,659.45</h4>
							</div>
						</div>
						<div className={styles.cardFooter}>
							<a href="">View all</a>
						</div>
					</div>


					<div className={styles.cardSingle}>
						<div className={styles.cardBody}>
							<span className={styles.tiReload}><img src="/reload.svg" alt="reload icon" /></span>
							<div>
								<h5>Pending</h5>
								<h4>$19,500.45</h4>
							</div>
						</div>
						<div className={styles.cardFooter}>
							<a href="">View all</a>
						</div>
					</div>

					<div className={styles.cardSingle}>
						<div className={styles.cardBody}>
							<span className={styles.tiCheckbox}><img src="/check-box.svg" alt="checkbox icon" /></span>
							<div>
								<h5>Processed</h5>
								<h4>$20,659</h4>
							</div>
						</div>
						<div className={styles.cardFooter}>
							<a href="">View all</a>
						</div>
					</div>

				</div>

				<div className={styles.sectionRecent}>
					<div className={styles.activityGrid}>
						<div className={styles.activityCard}>
							<h3>Recent Activity</h3>

							<div className={styles.tableResponsive}>


								<table>
									<thead>
										<tr>
											<th>Project</th>
											<th>Start Date</th>
											<th>End Date</th>
											<th>Team</th>
											<th>Status</th>
										</tr>
									</thead>

									<tbody>
										<tr>
											<td>App Development</td>
											<td>15 Aug, 2020</td>
											<td>22 Aug, 2020</td>
											<td className={styles.tdTeam}>
												<div className={styles.img1}></div>
												<div className={styles.img2}></div>
												<div className={styles.img3}></div>
											</td>
											<td>
												<span className={styles.badgeSuccess}>Success</span>
											</td>
										</tr>
										<tr>
											<td>Logo Design</td>
											<td>15 Aug, 2020</td>
											<td>22 Aug, 2020</td>
											<td className={styles.tdTeam}>
												<div className={styles.img1}></div>
												<div className={styles.img2}></div>
												<div className={styles.img3}></div>
											</td>
											<td>
												<span className={styles.badgeWarning}>Processing</span>
											</td>
										</tr>
										<tr>
											<td>Server Setup</td>
											<td>15 Aug, 2020</td>
											<td>22 Aug, 2020</td>
											<td className={styles.tdTeam}>
												<div className={styles.img1}></div>
												<div className={styles.img2}></div>
												<div className={styles.img3}></div>
											</td>
											<td>
												<span className={styles.badgeSuccess}>Success</span>
											</td>
										</tr>
										<tr>
											<td>Front-end Design</td>
											<td>15 Aug, 2020</td>
											<td>22 Aug, 2020</td>
											<td className={styles.tdTeam}>
												<div className={styles.img1}></div>
												<div className={styles.img2}></div>
												<div className={styles.img3}></div>
											</td>
											<td>
												<span className={styles.badgeWarning}>Processing</span>
											</td>
										</tr>
										<tr>
											<td>Web Development</td>
											<td>15 Aug, 2020</td>
											<td>22 Aug, 2020</td>
											<td className={styles.tdTeam}>
												<div className={styles.img1}></div>
												<div className={styles.img2}></div>
												<div className={styles.img3}></div>
											</td>
											<td>
												<span className={styles.badgeSuccess}>Success</span>
											</td>
										</tr>								
									</tbody>
							</table>

							</div>
							

						</div>

						<div className={styles.summary}>
							<div className={styles.summaryCard}>
								<div className={styles.summarySingle}>
									<span className={styles.tiIdBadge}><img src="/id-badge.svg" alt="badge" /></span>
									<div>
										<h5>196</h5>
										<small>Number of staff</small>
									</div>
								</div>
								<div className={styles.summarySingle}>
									<span className={styles.tiCalendar}><img src="/calendar.svg" alt="calendar" /></span>
									<div>
										<h5>16</h5>
										<small>Number of leave</small>
									</div>
								</div>
								<div className={styles.summarySingle}>
									<span className={styles.tiFaceSmile}><img src="/face-smile.svg" alt="face-smile" /></span>
									<div>
										<h5>12</h5>
										<small>Profile update request</small>
									</div>
								</div>
							</div>

							<div className={styles.bdayCard}>
								<div className={styles.bdayFlex}>
									<div className={styles.bdayImg}></div>
									<div className={styles.bdayInfo}>
										<h5>Dwayne F. Sanders</h5>
										<small>Birthday Today</small>	
									</div>
								</div>

								<div className={styles.textCenter}>
									<button>
										<span className={styles.tiGift}><img src="/gift.svg" alt="gift"/></span>
										Wish him
									</button>
								</div>

							</div>
						</div>
					</div>					
				</div>

			</div>
		</div>

		</div>

			


		)


}