import {useState,useEffect} from 'react'
import Record from '../../components/RecordsCard'
import AppHelper from '../../app_helper.js'
import {Fragment} from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import moment from 'moment'
import Sidebar from '../../components/sidebar/Sidebar'
import styles from './Records.module.css'

export default function Records() {

  //record state is for array of components
  const [records, setRecords] = useState([])
  const [searchItem, setSearchItem] = useState('')
  //recordData state is array of ALL records by user
  const [recordData, setRecordData] = useState([])
  //filteredData state is array of records filtered according to categoryType
  const [filteredData, setFilteredData] = useState([])
  //cateType is state for category Types.
  const [cateType, setCateType] = useState('All')

  const [balArr, setBalArr] = useState([])

  useEffect(() => {
    const option = {
      headers: {
        Authorization: `Bearer ${AppHelper.getAccessToken()}`
      }
    }

    fetch(`${AppHelper.API_URL}/users/details`, option)
    .then(AppHelper.toJSON)
    .then(data => {
      //console.log(data.firstName, data.lastName)
      //return <Component prop1={data.firstName} prop2={data.lastName} />
    })     

  },[])

  useEffect(() => {
    const option = {
      headers: {
        Authorization: `Bearer ${AppHelper.getAccessToken()}`
      }
    }

    fetch(`${AppHelper.API_URL}/users/records`, option)
    .then(AppHelper.toJSON)
    .then(data => {
      let amountArr = [];
      let balanceArr = [];
      const getBalance = (accumulator, currentValue) => accumulator + currentValue;
      data.forEach(element => {
        if(element.categoryType === "Expense"){
          amountArr.push(element.amount * -1);
        } else {
          amountArr.push(element.amount)
        }
        balanceArr.push(amountArr.reduce(getBalance))
      })
      balanceArr.reverse()
      data.reverse()
      const allRecords = data.map(record => {

        //console.log(record)
        const date = moment(record.dateMade).format('MMMM DD, YYYY')
          return <Record key={record._id} prop={record} date={date} balance={balanceArr[data.indexOf(record)]}/>
      })
      setRecords(allRecords)
      setRecordData(data)
    })     

  },[])

  useEffect(()=>{

    function showRecords(arr){
      let amountArr = [];
      let balanceArr = [];
      const getBalance = (accumulator, currentValue) => accumulator + currentValue;
      arr.forEach(element => {
        if(element.categoryType === "Expense"){
          amountArr.push(element.amount * -1);
        } else {
          amountArr.push(element.amount)
        }
        balanceArr.push(amountArr.reduce(getBalance))
      })
      setRecords(arr.map(record => {
        //console.log(record)
        const date = moment(record.dateMade).format('MMMM DD, YYYY')
          return <Record key={record._id} prop={record} date={date} balance={balanceArr[arr.indexOf(record)]}/>
      }))
    }
    setSearchItem('')
    if(cateType === 'All'){
      showRecords(recordData)
    } else {

      let empArr = []
      recordData.map(record => {
        //console.log(record)
        if(record.categoryType === cateType){
          empArr.push(record)
        }
      })
      //console.log(empArr)
      showRecords(empArr)
      setFilteredData(empArr)
    }
  },[cateType])

  useEffect(() => {
    function showRecords(arr){
      setRecords(arr.map(record => {
        //console.log(record)
        const date = moment(record.dateMade).format('MMMM DD, YYYY')
          return <Record key={record._id} prop={record} date={date}/>
      }))
    }
    //console.log(searchItem)
    let match = [];
    //console.log(recordData)
    if(searchItem !== ''){
      if(cateType === 'All'){
        recordData.forEach(record => {
          //console.log(record)
          if(record.description.toLowerCase().includes(searchItem.toLowerCase())){
            match.push(record)
          }
        })
        showRecords(match)

      } else {
        filteredData.forEach(record => {
          //console.log(record)
          if(record.description.toLowerCase().includes(searchItem.toLowerCase())){
            match.push(record)
          }
        })
        showRecords(match)
      }
      
    } else {
      if(cateType === 'All'){
        showRecords(recordData)
      } else {
        showRecords(filteredData)
      }
      
    }
   },[searchItem])



  return (

    <>
          <Sidebar />

          <div className={styles.mainContent}>
            <div className={styles.main}>

              <div className="pt-3 mb-5 container">
              <h3>Records</h3>
              <Form>    
                
                <Form.Group>
                <div className="mb-2 input-group">
                  <Button href="/records/new" className="btn btn-success">Add</Button>
                  <Form.Control value={searchItem}
                  type="text" 
                  placeholder="Search Record" 
                  onChange={e => setSearchItem(e.target.value)}/>
                  <Form.Control as="select" onChange={e => setCateType(e.target.value)}>
                    <option value="All" selected>All</option>
                    <option value="Income">Income</option>
                    <option value="Expense">Expense</option>
                  </Form.Control>
                </div>
                </Form.Group>
                
              </Form>
              {records}      
              </div>
          </div>
        </div>
    </>
    )
}

