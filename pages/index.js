import {Fragment} from 'react'
import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Banner from '../components/banner/Banner'
import Features from '../components/features/Features'
import About from '../components/about/About'
import Footer from '../components/footer/Footer'
import Container from 'react-bootstrap/Container'

export default function Home() {
  return (    
  	 

    <Fragment>

    	<div>
	      <Head>
	        <title>Budget Buddy</title>
	        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1" />
          <link rel="stylesheet" href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" />
          <link rel="preconnect" href="https://fonts.gstatic.com" />
          <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet" />
	      </Head>      
	    </div>
    	
    	<Banner />
    	<Features />
    	<About />
      <Footer />

    </Fragment>
  )
}
