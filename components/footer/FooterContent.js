import styles from './Footer.module.css'
import {Row,Col} from 'react-bootstrap'
import Container from 'react-bootstrap/Container'
import Image from 'next/image'
import { FcPhone } from "react-icons/fc";
import { FaPhoneAlt } from "react-icons/fa";
import { IoIosMail } from "react-icons/io";
import { SiFacebook } from "react-icons/si";
import { SiLinkedin } from "react-icons/si";



export default function FooterContent() {

	return (


		<div className={styles.footer}>
			<div className={styles.footerInfo}>
				<div className={styles.footerAbout}>
					<h3>Budget Buddy</h3>

					<p>Track your income and expense using this app. </p>

					<div className={styles.socialMedia}>
						<ul>
							<li><a href=""><i class={styles.socialI}><SiFacebook /></i></a></li>
							<li><a href=""><i class={styles.socialI}><SiLinkedin /></i></a></li>
						</ul>	
					</div>
				</div>

				<div className={styles.footerLink}>
					<h5>Quick Links</h5>
					<ul>
						<li><a href="#">Home</a></li>
						<li><a href="#features-content">Features</a></li>
						<li><a href="#about-content">About</a></li>
						<li><a href="#contact-us">Contact</a></li>
						<li><a href="/login">Login</a></li>
					</ul>

				</div>

				<div className={styles.footerContact}>
					<h5>Contacts</h5>
					<ul>
						<li>
							<span><i class={styles.contactSpan}><IoIosMail /></i></span>
							<a href="mailto:icebenitez@gmail.com">icebenitez@gmail.com</a>
						</li>
						<li>
							<span><i class={styles.contactSpan}><FaPhoneAlt /></i></span>
							<a href="">+63 9755736020</a>
						</li>
						<br />					
						<li>
							<span><i class={styles.contactSpan}><IoIosMail /></i></span>
							<a href="mailto:anthony.castro0022@gmail.com">anthony.castro0022@gmail.com</a>
						</li>
						
						<li>
							<span><i class={styles.contactSpan}><FaPhoneAlt /></i></span>
							<a href="">+63 9274406803</a>
						</li>

					</ul>
				</div>
				<div className={styles.footerApps}>
					<h5>This Website is Powered by:</h5>
					<Row>
						<Col>
							<img src="/gitlab.svg" className={styles.aboutImg} />
						</Col>
						<Col>
							<img src="/vercel.png" className={styles.aboutImg} />
						</Col>
					</Row>
					
					
				</div>


			</div>
			<div className={styles.copyright}>
				<p className={styles.copyrightP}>&copy; All Rights Reserved 2021 | Benitez & Castro</p>
			</div>
		</div>





		)


}

