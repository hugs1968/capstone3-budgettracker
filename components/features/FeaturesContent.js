import styles from './Features.module.css'
import {Row,Col} from 'react-bootstrap'
import Container from 'react-bootstrap/Container'
import Image from 'next/image'
import Card from 'react-bootstrap/Card'
import CardGroup from 'react-bootstrap/CardGroup'

export default function FeaturesContent() {

	return (

		<>
		<div id="features-content" className={styles.featuresContent} >
		<Container >
			<Row >					
				<Col md={12}>
					<h2 className={styles.featuresH2}>
						OUR EXCITING FEATURES
					</h2>
					<h4 className={styles.featuresH4}>Ideal Solutions For You
					</h4>					
				</Col>				
			</Row>


			<Row>
				<Col md={4}>
					<Image className={styles.featureSVG1} src="/wallet.svg" alt="wallet" width={100}
        height={100}/>
        			<h6>Helps You Track Your Budget</h6>
        			<p>Be able to create a list of your income and expenses as a record.</p>
            
				</Col>
				<Col md={4}>
					<Image className={styles.featureSVG1} src="/features-chart.svg" alt="chart" width={100}
        height={100}/>
        			<h6>Charts and Graph</h6>
        			<p>You can view your income and expense summary through charts and graph.</p>
            
				</Col>
				<Col md={4}>
					<Image className={styles.featureSVG1} src="/success.svg" alt="success" width={100}
        height={100}/>
        			<h6>Dashboard Feature</h6>
        			<p>You will have your own dashboard to view your progress with budgeting.</p>
            
				</Col>
			</Row>





		</Container>
		</div>
		</>



		)



}