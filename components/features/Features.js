import styles from './Features.module.css'
import Container from 'react-bootstrap/Container'
import FeaturesContent from './FeaturesContent'

export default function Features() {

	return (

		<>
		<div fluid id="section-features" className={styles.sectionFeatures}>
				
				<FeaturesContent />


		</div>
		</>


		)



}
