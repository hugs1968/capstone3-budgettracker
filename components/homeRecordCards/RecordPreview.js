import {useState, useEffect} from 'react'
import styles from './RecordPreview.module.css'
import Card from 'react-bootstrap/Card'
import {Row, Col} from 'react-bootstrap'
import {Fragment} from 'react'

export default function HomeRecordPreview({prop, date}){

	const {_id, description, categoryType, categoryName, amount, balance} = prop
	//console.log(description)
	return(
		<Fragment>
			
									{
										categoryType === "Expense"
										?
										<div className={styles.customer}>
										<div className={styles.info}>
											<span className={styles.statusRed}></span>	
											<div>
												<h4 className={styles.description}>{description}</h4>
												<small>{date}</small>												
											</div>
										</div>
										<div className={styles.contact}>
											<span>- $ {amount}</span>

										</div>
										<hr />																				
										</div>																			
										:
										<div className={styles.customer}>
										<div className={styles.info}>
											<span className={styles.statusGreen}></span>	
											<div>
												<h4 className={styles.description}>{description}</h4>
												<small>{date}</small>
											</div>
										</div>
										<div className={styles.contact}>
											<span>+ $ {amount}</span>
										</div>
										<hr />
										</div>
									}
									




		</Fragment>


		)

}