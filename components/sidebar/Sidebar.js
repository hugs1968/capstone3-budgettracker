import styles from './Sidebar.module.css'
import {Fragment} from 'react'
import { FaUnlink } from 'react-icons/fa'
import Image from 'next/image'
import { MdDashboard } from 'react-icons/md'
import { IoHome } from "react-icons/io5";
import { IoListCircle } from "react-icons/io5";
import { IoBarChartSharp } from "react-icons/io5";
import { ImList2 } from "react-icons/im";
import { ImIndentDecrease } from "react-icons/im";
import { MdLibraryBooks } from 'react-icons/md'
import { FaCalendarMinus } from "react-icons/fa";
import { GiChart } from "react-icons/gi";
import { BsFillPieChartFill } from "react-icons/bs";


export default function Sidebar() {

	return (

	<Fragment>

	
	<input type="checkbox" id="sidebarToggle" className={styles.sidebarToggle} />
		<div className={styles.sidebar}>
			<div className={styles.sidebarHeader}>
				<h4 class={styles.brand}>					
					<span>Budget Buddy</span>
					
				</h4> 
				<label htmlFor="sidebarToggle"><MdDashboard /></label>
			</div>
				
			<div className={styles.sidebarMenu}>

				<ul>
					<li>
						<a href="/dashboard">
							<IoHome />
							<span>Home</span>
						</a>
					</li>
					<li>
						<a href="/categories">
							<ImList2 />
							<span>Categories</span>
						</a>
					</li>
					<li>
						<a href="/records">
							<MdLibraryBooks />
							<span>Records</span>
						</a>
					</li>
					<li>
						<a href="/charts/monthly-income">
							<IoBarChartSharp />
							<span>Monthly Income</span>
						</a>
					</li>
					<li>
						<a href="/charts/monthly-expense">
							<FaCalendarMinus />					
							<span>Monthly Expense</span>
						</a>
					</li>
					<li>
						<a href="/charts/balance-trend">
							<GiChart />
							<span>Trend</span>
						</a>
					</li>
					<li>
						<a href="/charts/category-breakdown">
							<BsFillPieChartFill />
							<span>Budget Breakdown</span>
						</a>
					</li>
					<li>
						<a href="/logout">
							<span>Logout</span>
						</a>
					</li>					
				</ul>
			</div>
		</div>




		</Fragment>
		)



}