import {useState,useEffect,useContext} from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Card from 'react-bootstrap/Card'
import {Row,Col} from 'react-bootstrap'
import UserContext from '../../UserContext'
import AppHelper from '../../app_helper.js'
import Swal from 'sweetalert2'
import Router from 'next/router'

import Modal from 'react-bootstrap/Modal'
import ModalDialog from 'react-bootstrap/ModalDialog'
import ModalHeader from 'react-bootstrap/ModalHeader'
import ModalTitle from 'react-bootstrap/ModalTitle'
import ModalBody from 'react-bootstrap/ModalBody'
import ModalFooter from 'react-bootstrap/ModalFooter'
import styles from './Popup.module.css'


export default function Popup(props) {

  const {user} = useContext(UserContext)
  const handleClose = () => setOpenPopup(false);

  const {title, children, openPopup, setOpenPopup} = props;


  const [categoryName,setCategoryName] = useState('')
  const [categoryType,setCategoryType] = useState('')
  const [isActive,setIsActive] = useState(false)



useEffect(()=>{

  if(categoryName !== '' && categoryType !== ''){

    setIsActive(true)

  } else {

    setIsActive(false)

  }

})


function createCategory(e) {

  e.preventDefault()

  const payload = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${AppHelper.getAccessToken()}`
    },
    body: JSON.stringify({
      categoryName: categoryName,
      categoryType: categoryType
    })
  }

  fetch(`${AppHelper.API_URL}/users/add-category/`, payload)
  .then(res => res.json())
  .then(data => {
    console.log(data)
      if(data){ 
        Swal.fire({
        icon: 'success',
        title: "Category added"
        })
        Router.push('/categories')
      } else {
        Swal.fire({
        icon: 'error',
        title: "There was an error"
        })        

      }  
  })

  //console.log(`The New Category has been added`)

  setCategoryName('')
  setCategoryType('')

}


  return (

    <Modal show = {openPopup} onHide={handleClose}>        
          <div className={styles.modalTitle}>
          <Modal.Title >
              <h5>Category Information Form</h5>
          </Modal.Title>   
          </div>     
          <hr />
        <Modal.Body className={styles.cardCategory}>            
              <Form onSubmit={(e)=> createCategory(e)}>
                <Form.Group className="mb-4">
                  <Form.Label className={styles.categoryLabel}>
                    Category Name <span>(e.g. Allowance, Bills)</span>
                  </Form.Label>
                  <Form.Control type="text" value={categoryName} onChange={e => setCategoryName(e.target.value)} required className="form-control" />
                </Form.Group>
                <Form.Group>
                  <Form.Label className={styles.categoryLabel}>
                    Category Type <span>(e.g. Income or Expense)</span>
                  </Form.Label>
                  <select value={categoryType} onChange={e => setCategoryType(e.target.value)} required className="form-control">
                    <option className="categoryName" value="true" disabled=""> </option>                    
                    <option value="Income">Income</option>
                    <option value="Expense">Expense</option>
                  </select>
                </Form.Group>
                <button type="submit" className={styles.btnSubmit}>Submit</button>
                <button className={styles.btnClose} onClick={handleClose}><a className={styles.categoryPush} href="/categories">Close</a>
                </button>
              </Form>             
            
        </Modal.Body>              
      </Modal>

    )

}

