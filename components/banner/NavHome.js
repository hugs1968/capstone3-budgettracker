import Nav from 'react-bootstrap/Nav'
import Navbar from 'react-bootstrap/Navbar'
import Button from 'react-bootstrap/Button'
import styles from './Banner.module.css'
import Container from 'react-bootstrap/Container'
import {useContext} from 'react'
import UserContext from '../../UserContext'


export default function NavHome() {

  const {user} = useContext(UserContext)
  

  return (



<Container>
  <Navbar variant="light" expand="lg">
  <Navbar.Brand className={styles.navBrand} href="/">Budget Buddy</Navbar.Brand>
  <Navbar.Toggle />
  <Navbar.Collapse className="justify-content-end">
    {
      
      user.email
      ?
      <>
      <Nav.Link className={styles.navlink} href="/">Home</Nav.Link>
      <Nav.Link className={styles.navlink} href="#features-content">Features</Nav.Link>
      <Nav.Link className={styles.navlink} href="#about-content">About</Nav.Link>
      <Nav.Link className={styles.navlink} href="#contact-us">Contact Us</Nav.Link>
      <Nav.Link className={styles.navlink} href="/dashboard">Dashboard</Nav.Link>
      <Nav.Link className={styles.navlink} href="/logout">Logout</Nav.Link>
      </>
      :
      <>
      <Nav.Link className={styles.navlink} href="/">Home</Nav.Link>
      <Nav.Link className={styles.navlink} href="#features-content">Features</Nav.Link>
      <Nav.Link className={styles.navlink} href="#about-content">About</Nav.Link>
      <Nav.Link className={styles.navlink} href="#contact-us">Contact Us</Nav.Link>
      <Nav.Link className={styles.navlink} href="/login">Login</Nav.Link>
      <Button variant="mr-2" className={styles.navRegisterButton} href="/register">Register</Button>
      </>
    }
    
  </Navbar.Collapse>
</Navbar>
</Container>


  )




}

