import styles from './Banner.module.css'
import NavHome from './NavHome'
import BannerContent from './BannerContent'
import Container from 'react-bootstrap/Container'


export default function Banner() {

	return (
		<>
		<div fluid id="section-header" className={styles.sectionHeader}>				
				<NavHome />
				<BannerContent />
		</div>
		</>

		)



}