import styles from './Banner.module.css'
import {Row,Col} from 'react-bootstrap'
import Container from 'react-bootstrap/Container'
import Image from 'next/image'


export default function BannerContent() {

	return (

		<>
		<div id="section-content" >
		<Container >
			<Row className={styles.sectionContent}>					
				<Col md={4}>					
					<h1 className="mb-3">
						The Trending No.1 <br />Personal Finance App
					</h1>
					<p className="mb-4">
						It's time to take control of your spending habits. Track your income and expense using this app.
					</p>					
					<a href="/" className={styles.registerButton}>Register Now</a>
				</Col>
				<Col md={8}>
					<Image className={styles.bannerImage} layout="responsive" src="/banner-img2.png" alt="banner image" width={100}
        height={100}/>
				</Col>
			</Row>
		</Container>
		</div>

		</>



		)



}