import styles from './Sidebar2.module.css'
import {useState,useEffect,useContext} from 'react'
import AppHelper from '../../app_helper.js'
import UserContext from '../../UserContext'
import HomeRecordPreview from '../../components/homeRecordCards/RecordPreview'
import moment from 'moment'
import PieBreakdown from '../charts/PieChart'
import LineChartTrend from '../charts/LineChart'
import {Pie} from 'react-chartjs-2'
import Image from 'next/image'

import { FaUnlink } from 'react-icons/fa'
import { MdDashboard } from 'react-icons/md'
import { IoHome } from "react-icons/io5";
import { IoListCircle } from "react-icons/io5";
import { IoBarChartSharp } from "react-icons/io5";
import { ImList2 } from "react-icons/im";
import { ImIndentDecrease } from "react-icons/im";
import { MdLibraryBooks } from 'react-icons/md'
import { FaCalendarMinus } from "react-icons/fa";
import { GiChart } from "react-icons/gi";
import { GiHamburgerMenu } from "react-icons/gi";
import { BsFillPieChartFill } from "react-icons/bs";
import { BsArrowRightShort } from "react-icons/bs";
import { BsFillCaretDownFill } from "react-icons/bs";
import { BiLogOut } from "react-icons/bi";
import { FiMenu } from "react-icons/fi";
import { VscSearch } from "react-icons/vsc";

export default function Sidebar2() {

const {user} = useContext(UserContext)
const [userInfo, setUserInfo] = useState([])
const [totalBalance,setTotalBalance] = useState(0)
const [latestIncome,setLatestIncome] = useState(0)
const [latestExpense,setLatestExpense] = useState(0)
const [firstName,setFirstName] = useState('')
const [lastName,setLastName] = useState('')
const [names, setNames] = useState([])
const [dataPerCategories, setDataPerCategories] = useState([])

const [records, setRecords] = useState([])
const [allRecords, setAllRecords] = useState([])
const [categoryData, setCategoryData] = useState([])
const [trendData, setTrendData] = useState([])
const [dateFrom, setDateFrom] = useState('')
const [dateTo, setDateTo] = useState('')

useEffect(()=> {
	let today = new Date();
	//console.log(today)
	setDateTo(moment(today).format('YYYY-MM-DD'))
	setDateFrom(moment(today).subtract(31, 'days').format('YYYY-MM-DD'))
},[])

//console.log(dateFrom)
//console.log(dateTo)

useEffect(() => {
const option = {
  headers: {
    Authorization: `Bearer ${AppHelper.getAccessToken()}`
  }
}

fetch(`${AppHelper.API_URL}/users/records`, option)
.then(AppHelper.toJSON)
.then(data => {
	//this will get the latest balance
	setTotalBalance(data[data.length-1].balance)
	let incomeArr = data.filter(e => e.categoryType === "Income")
	//
	setLatestIncome(incomeArr[incomeArr.length-1].amount)
	let expenseArr = data.filter(e => e.categoryType === "Expense")
	setLatestExpense(expenseArr[expenseArr.length-1].amount)
	setAllRecords(data)
  //data.reverse()
  const allRecords = data.map(record => {
    //console.log(record)
    	const date = moment(record.dateMade).format('MMMM DD, YYYY')
     	return <HomeRecordPreview key={record._id} prop={record} date={date} />
  })
  setRecords([allRecords[allRecords.length-1], allRecords[allRecords.length-2], allRecords[allRecords.length-3], allRecords[allRecords.length-4], allRecords[allRecords.length-5], allRecords[allRecords.length-6]])

})

},[])

//console.log(allRecords)

useEffect(() => {			
	let recentRecord = []
	allRecords.forEach(record => {
		let recordDate = moment(record.dateMade).format('YYYY-MM-DD')
		if(recordDate >= dateFrom && recordDate <= dateTo){
			recentRecord.push(record.balance)
		}
	})
	//console.log(recentRecord)
	setTrendData(recentRecord)
},[allRecords])

useEffect(() => {
    const option = {
      headers: {
        Authorization: `Bearer ${AppHelper.getAccessToken()}`
      }
    }

    fetch(`${AppHelper.API_URL}/users/details`, option)
    .then(AppHelper.toJSON)
    .then(data => {
    	//console.log(data)

    	setFirstName(data.firstName)
    	setLastName(data.lastName)
         
      })
    	
  },[])



	return (
			<>

			<input type="checkbox" id="navToggle" className={styles.navToggle} />
			<div className={styles.sidebar}>
				<div className={styles.sidebarBrand}>
					<h6><span><BsFillPieChartFill /></span> <span>Budget Buddy</span></h6>
				</div>

				<div className={styles.sidebarMenu}>
					<ul>
						<li>
							<a href="/dashboard" className={styles.activeDashboard}><span className={styles.menu}><IoHome /></span><span>Dashboard</span></a>
						</li>						
						<li>
							<a href="/categories" className={styles.activeCategories}><span className={styles.menu}><ImList2 /></span><span>Categories</span></a>
						</li>
						<li>
							<a href="/records" className={styles.activeRecords}><span className={styles.menu}><MdLibraryBooks /></span><span>Records</span></a>
						</li>
						<li>
							<a href="/charts/monthly-income" className={styles.activeIncome}><span className={styles.menu}><IoBarChartSharp /></span><span>Monthly Income</span></a>
						</li>
						<li>
							<a href="/charts/monthly-expense" className={styles.activeExpense}><span className={styles.menu}><FaCalendarMinus />	</span><span>Monthly Expense</span></a>
						</li>
						<li>
							<a href="/charts/balance-trend" className={styles.activeTrend}><span className={styles.menu}><GiChart /></span><span>Trend</span></a>
						</li>
						<li>
							<a href="/charts/category-breakdown" className={styles.activeBreak}><span className={styles.menu}><BsFillPieChartFill /></span><span>Budget Breakdown</span></a>
						</li>
						<li className="mt-5 pt-5">
							<a href="/charts/category-breakdown" className={styles.activeBreak}><span className={styles.menu}><BiLogOut /></span><span>Logout</span></a>
						</li>
					</ul>
				</div>
			</div>

			<div className={styles.mainContent}>
				<div className={styles.header}>

						<h2>
							<label htmlFor="navToggle">
								<span className={styles.laBars}><FiMenu /></span>
							</label>

							Dashboard
						</h2>

						<div className={styles.userWrapper}>
							<img src="/avatar.png" alt="" className="" width="40px" height="40px" />
							<div>
								<h4>{firstName} {lastName}</h4>
								<small>Super admin</small>															
							</div>
							<div className={styles.userDropdown}>
				          		<button><BsFillCaretDownFill className={styles.dropdownButton} /></button>
					          <ul>					            
						        <li><a href="/logout">Logout</a></li>	
					          </ul>
					        </div>						
						</div>	
				</div>

				<div className={styles.main}>
					<div className={styles.cards}>

						<div className={styles.cardSingle}>
							<div>
								<h3>$ {totalBalance}</h3>
								<span>Balance</span>
							</div>
							<div>
								<span className={styles.laGoogleWallet}><IoBarChartSharp /></span>
							</div>
						</div>

						<div className={styles.cardSingle}>
							<div>
								<h5>$ {latestIncome}</h5>
								<span>Income</span>
							</div>
							<div>
								<span className={styles.laUsers}><IoBarChartSharp /></span>
							</div>
						</div>

						<div className={styles.cardSingle}>
							<div>
								<h5>$ {latestExpense}</h5>
								<span>Expense</span>
							</div>
							<div>
								<span className={styles.laUsers}><IoBarChartSharp /></span>
							</div>
						</div>

						<div className={styles.cardSingle}>
							<div>
								<h6 className="text-center">Your Budget Chart for this month</h6>								
							</div>	
							<div>
								<span className={styles.laUsers}><PieBreakdown data={categoryData} /></span>
							</div>						
						</div>

						
					</div>

					<div className={styles.recentGrid}>
						<div className={styles.projects}>
							<div className={styles.card}>
								<div className={styles.cardHeader}>
									<h3>Budget Chart</h3>

									<button>See all<span><BsArrowRightShort /></span></button>
								</div>
								<div className={styles.cardBody}>
									
								{<LineChartTrend data={trendData}/>}

								</div>
							</div>
						</div>
						<div className={styles.customers}>
							<div className={styles.card}>
								<div className={styles.cardHeader}>
									<h3>Records</h3>

									<a href="/records"><button>See all<span><BsArrowRightShort /></span></button></a>
								</div>	

								<div className={styles.cardBody}>
									{records}


									
								</div>
							
							</div>
						</div>
					</div>



				</div>



			</div>

			</>
			)

}

