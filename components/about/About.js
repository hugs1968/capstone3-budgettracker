import styles from './About.module.css'
import Container from 'react-bootstrap/Container'
import AboutContent from './AboutContent'

export default function About() {

	return (

		<>
		<div fluid id="section-about" className={styles.sectionAbout}>
				
				<AboutContent />


		</div>
		</>


		)



}