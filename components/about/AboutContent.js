import styles from './About.module.css'
import {Row,Col} from 'react-bootstrap'
import Container from 'react-bootstrap/Container'
import Image from 'next/image'
import Card from 'react-bootstrap/Card'
import CardDeck from 'react-bootstrap/CardDeck'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFacebook } from "@fortawesome/free-brands-svg-icons"
import { faLinkedin } from "@fortawesome/free-brands-svg-icons"
import { faHome } from "@fortawesome/free-solid-svg-icons";
import { SiFacebook } from "react-icons/si";
import { SiLinkedin } from "react-icons/si";

export default function AboutContent() {

	return (

		<>
		<div id="about-content" className={styles.aboutContent} >
		<Container className="mt-5">

		<Row>					
			<Col md={12}>
				<h2 className={styles.aboutH2}>
					ABOUT US
				</h2>
				<h4 className={styles.aboutH4}>Learn about the developers and technologies behind this amazing app.
				</h4>					
			</Col>				
		</Row>






		<Row>
			<Col>	
			
			<img src="/nextjs.svg" className={styles.aboutImg} />
			<img src="/react.svg" className={styles.aboutImg} />
			<img src="/mongodb.svg" className={styles.aboutImg} />
			<img src="/nodejs.png" className={styles.aboutImg} />
			<img src="/express.svg" className={styles.aboutImg} />


			</Col>
		</Row>



		</Container>
</div>

	
		<div className={styles.sectionTeam}>
		<Container>
			<h3 className={styles.headingTeam}>Meet the Team</h3>
			<div className={styles.cardWrapper}>
				<div className={styles.cardTeam}>
					<img className={styles.teamImage}src="/prof-bg1.jpg" alt="card background" width={500} height={416} />
					<img className={styles.profileImage} src="/ice.jpg" alt="profile image" width={240} height={240} style={{zIndex: 999}}  />
					<h1 className={styles.h1Team}>Jason Benitez</h1>
					<p className={styles.jobTitle}>Full Stack Developer</p>
					<p className={styles.aboutTeam}>
					Ice is interested with REST API and React.
					</p>
					<a className={styles.aButton} href="mailto:icebenitez@gmail.com" variant="primary">Contact</a>
					<ul className={styles.socialMedia}>
						<li><a href=""><i class={styles.socialI}><SiFacebook /></i></a></li>
						<li><a href=""><i class={styles.socialI}><SiLinkedin /></i></a></li>
						
					</ul>
				</div>
			


				<div className={styles.cardTeam} id="contact-us">
					<img className={styles.teamImage}src="/prof-bg2.jpg" alt="card background" width={500} height={416} />
					<img className={styles.profileImage}src="/ton.png" alt="profile image" width={240} height={240} style={{zIndex: 999}}  />
					<h1 className={styles.h1Team}>Anthony Castro</h1>
					<p className={styles.jobTitle}>Full Stack Developer</p>
					<p className={styles.aboutTeam}>
					Anthony is great with UI/UX development
					</p>
					<a className={styles.aButton} href="mailto:anthony.castro0022@gmail.com" variant="primary">Contact</a>
					<ul className={styles.socialMedia}>
						<li><a href=""><i class={styles.socialI}><SiFacebook /></i></a></li>
						<li><a href=""><i class={styles.socialI}><SiLinkedin /></i></a></li>
						
					</ul>
				</div>

			</div>

		</Container>
		</div>










		
		</>



		)



}
