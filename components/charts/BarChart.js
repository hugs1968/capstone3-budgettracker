import {useState, useEffect} from 'react'
import {Bar} from 'react-chartjs-2'
import moment from 'moment'
import {colorRandomizer} from '../../helpers/colorRandomizer'


export default function BarChart({data}){

	const months = ['January','February','March','April','May','June','July','August','September','October','November','December']
	const [monthlyData, setMonthlyData] = useState([])
	const [bgColors,setBgColors] = useState([])

	useEffect(()=>{

		//Monthly expense will then be populated by the accumulated expense for the specific month.
		setMonthlyData(months.map(month => {

			let amount = 0

			data.forEach(element => {

				//console.log(moment(element.sale_date).format("MMMM"))
				if(moment(element.dateMade).format('MMMM') === month){

					amount = amount + parseInt(element.amount)

				} else {
					return 0
				}

			})

			return amount

		}))
		
		setBgColors(months.map(() => `#${colorRandomizer()}`))

	},[data])

	//console.log(monthlyData)

	return(

			<Bar data = {{

				datasets:[{
					label: "Monthly Data",
					data: monthlyData,//array of numbers
					backgroundColor: bgColors
				
				}],
				labels: months
			}} />
		)
}