import {Line} from 'react-chartjs-2'

export default function LineChartHome({data}){

	//console.log(data)
	//console.log(dates)

	return(

			<Line data = {{

				datasets:[{
					label: "Balance Trend",
					data: data,//array of numbers
					backgroundColor: ['rgba(255, 99, 132, 0.2)'],
					borderColor: ["lightblue"],
					pointBorderColor: ["red"]
				
				}]

			}} />

		)

}