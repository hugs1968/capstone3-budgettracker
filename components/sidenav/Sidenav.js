import styles from './Sidenav.module.css'
import { FaUnlink } from 'react-icons/fa'
import Image from 'next/image'
import { MdDashboard } from 'react-icons/md'
import { IoHome } from "react-icons/io5";
import { IoListCircle } from "react-icons/io5";
import { IoBarChartSharp } from "react-icons/io5";
import { ImList2 } from "react-icons/im";
import { ImIndentDecrease } from "react-icons/im";
import { MdLibraryBooks } from 'react-icons/md'
import { FaCalendarMinus } from "react-icons/fa";
import { GiChart } from "react-icons/gi";
import { GiHamburgerMenu } from "react-icons/gi";
import { BsFillPieChartFill } from "react-icons/bs";
import { BsArrowRightShort } from "react-icons/bs";
import { FiMenu } from "react-icons/fi";
import { VscSearch } from "react-icons/vsc";








export default function Sidenav() {

	return (
			<>

			<input type="checkbox" id="navToggle" className={styles.navToggle} />
			<div className={styles.sidebar}>
				<div className={styles.sidebarBrand}>
					<h2><span className={styles.accusoftLogo}><BsFillPieChartFill /></span> <span>Budget Buddy</span></h2>
				</div>

				<div className={styles.sidebarMenu}>
					<ul>
						<li>
							<a href="" className={styles.active}><span className={styles.menu}><IoHome /></span><span>Dashboard</span></a>
						</li>						
						<li>
							<a href=""><span className={styles.menu}><ImList2 /></span><span>Categories</span></a>
						</li>
						<li>
							<a href=""><span className={styles.menu}><MdLibraryBooks /></span><span>Records</span></a>
						</li>
						<li>
							<a href=""><span className={styles.menu}><IoBarChartSharp /></span><span>Monthly Income</span></a>
						</li>
						<li>
							<a href=""><span className={styles.monthlyExpense}><FaCalendarMinus />	</span><span>Monthly Expense</span></a>
						</li>
						<li>
							<a href=""><span className={styles.menu}><GiChart /></span><span>Trend</span></a>
						</li>
						<li>
							<a href=""><span className={styles.menu}><BsFillPieChartFill /></span><span>Budget Breakdown</span></a>
						</li>
					</ul>
				</div>
			</div>



			</>
			)

}

